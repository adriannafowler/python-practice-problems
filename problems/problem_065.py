# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function
#------------------------------------------------------------
#input: a list of numbers --> must have at least 2 nums
#output: the largest gap between any two nums in the list
#ALWAYS a positive num

def biggest_gap(list_nums):
    max_gap = 0
    for i in range(1, len(list_nums) - 1):
        gap = abs(list_nums[i + 1] - list_nums[i])
        if gap > max_gap:
            max_gap = gap
        i += 1
    return max_gap

        # num1 = list_nums[index]
        # num2 = list_nums[(index +1)]
        # if num1 > num2:
        #     dif = num1 - num2
        #     list_difs.append(dif)
        # elif num2 > num1:
        #     dif = num2 - num1
        #     list_difs.append(dif)
        # index += 1
    print(list_difs)

print(biggest_gap([1, 11, 9, 20, 0]))


# def biggest_gap(nums):                          # solution
#     max_gap = abs(nums[1] - nums[0])            # solution
#     for i in range(1, len(nums) - 1):           # solution
#         gap = abs(nums[i + 1] - nums[i])        # solution
#         if gap > max_gap:                       # solution
#             max_gap = gap                       # solution
#     return max_gap
