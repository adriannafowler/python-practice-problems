# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#
#----------------------------------------------------

#input: list of numbers
#find which number the greatest
#output: max value

def max_in_list(*values):
    if values == []:
        return None
    else:
        values = list(values)
        values.sort()
    return values[len(values) - 1]

print(max_in_list(500000, 2, 15, 4, 5, 6855555555, -1, 0))
