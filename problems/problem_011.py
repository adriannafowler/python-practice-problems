# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
#-----------------------------------------------------

#use modulus to determine if divided by 3 remainder is 0
#returns "fizz" if true

def is_divisible_by_5(number):
    #if statement for number mod 5
    if number % 5 == 0:
        return "buzz"
    else:
        return number


print(is_divisible_by_5(123))
