# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4
#--------------------------------------------------

#input: a number
#output: the sum of the fractions of the
#        form 1/2+2/3+3/4+...+number/number+1

from fractions import Fraction

def sum_fraction_sequence(number):
    #list of fractions
    lst_fractions = []
    # a and b counts
    a = 1
    b = 2
    #add this number of fractions to equation
    for i in range(number):
        fraction = a / b
        lst_fractions.append(fraction)
        a += 1
        b += 1
    #print(lst_fractions)
    sum = 0
    for frx in lst_fractions:
        sum += frx
    return sum


print(sum_fraction_sequence(3))
