# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2
#---------------------------------------------------------

#input: one string
#output: 2 values --> number of letters in string,
#and number of digits in string


#create 2 lists -- letters, numbers
#if and elif statement to check if letter or number
#append to appropriate string
#return len of each list


def count_letters_and_digits(s):
    letters = []
    numbers = []
    for i in s:
        if i.isdigit() == True:
            numbers.append(i)
        elif i.isalpha() == True:
            letters.append(i)
    return f"letters: {len(letters)}\
                 numbers: {len(numbers)}"

print(count_letters_and_digits("a1"))
