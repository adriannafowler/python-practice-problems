# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.
#----------------------------------------------------------------

#input:a list and a search term
#output: new list that contains indexes for search term

def find_indexes(search_list, search_term):
    search_term_index = []
    enum_list = list(enumerate(search_list))

    for i in enum_list:
        if int(i[1]) == search_term:
            search_term_index.append(i[0])
    return search_term_index


print(find_indexes([1, 2, 1, 2, 1], 1))
