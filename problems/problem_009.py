# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
#------------------------------------------------------------

#is word same forwards and reversed? .reversed str method
#return True or False
# use reverse func
#use join method


def is_palindrome(word:str):
    #turn input to str
    rev_word = word[::-1]
    word = word.lower()
    rev_word = rev_word.lower()

    #if statement for is word and word reversed same?
    if word == rev_word:
        return True
    else:
        return False

print(is_palindrome("Hannah"))
