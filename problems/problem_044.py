# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.
#------------------------------------------------------------

#input: a list of keys, a dictionary
#output: new list of corresponding values to keys entered in og list
#if key in og list does not exist in dictionary, new list should say None for that key

def translate(key_list, dictionary):
    values_list = []
    for item in key_list:
        values_list.append(dictionary.get(item))
    return values_list


print(translate(["age", "age", "age"], {"name":"Noor", "age": 29}))
