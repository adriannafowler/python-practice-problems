# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.
#---------------------------------------------------------

#input: 2 lists of same size
#add same index of each list and assign sum to new list
#output: new list

def pairwise_add(list1, list2):
    if len(list1) != len(list2):
        raise ValueError("list1 and list2 must contain same number of items")
    sum_list = []
    index = 0
    for i in list1:
        sum = list1[index] + list2[index]
        sum_list.append(sum)
        index += 1
    return sum_list

print(pairwise_add([100, 200, 300], [10, 1, 180]))

print("----------------------------------------")

def pairwise_add2(list1, list2):
    if len(list1) != len(list2):
        raise ValueError("list1 and list2 must contain same number of items")
    sum_list = []
    index = 0
    list3 = list(zip(list1, list2))
    for i in list3:
        sum = list3[index][0] + list3[index][1]
        sum_list.append(sum)
        index += 1
    return(sum_list)

print(pairwise_add2([1, 2, 3, 4], [4, 5, 6, 7]))
