# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"
#---------------------------------------------------------------

#if day not sunny AND is workday, append "umbrella" to list
#if is workday, append "laptop" to list
#if not workday, append "surfboard"
#paramenters True, False

def gear_for_day(is_workday, is_sunny):
    gear_list = []
    if is_sunny == False and is_workday == True:
        gear_list.append("umbrella")
    if is_workday == True:
        gear_list.append("laptop")
    if is_workday == False:
        gear_list.append("surfboard")
    return print(gear_list)

gear_for_day(True, False)
