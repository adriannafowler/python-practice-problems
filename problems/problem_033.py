# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
#----------------------------------------------------------

#input: 1 number --> numercial count n
#output: sum of the first n even numbers
#use index (n*2)+1 to get a number that includes those even nums
#append even numbers to new list
#get sum of nums on even num list

def sum_of_first_n_even_numbers(n):
    if n < 0:
        return None
    nums = []
    even_nums = []
    for i in range((n * 2) + 1):
        nums.append(i)
    print(nums)
    for num in nums:
        if num % 2 == 0:
            even_nums.append(num)
    print(even_nums)
    sum = 0
    for even_num in even_nums:
        sum += even_num
    return sum

print(sum_of_first_n_even_numbers(-1))
