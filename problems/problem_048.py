# Write a function that meets these requirements.
#
# Name:       count_word_frequencies
# Parameters: sentence, a string
# Returns:    a dictionary whose keys are the words in the
#             sentence and their values are the number of
#             times that word has appeared in the sentence
#
# The sentence will contain no punctuation.
#
# This is "case sensitive". That means the word "Table" and "table"
# are considered different words.
#
# Examples:
#    * sentence: "I came I saw I learned"
#      result:   {"I": 3, "came": 1, "saw": 1, "learned": 1}
#    * sentence: "Hello Hello Hello"
#      result:   {"Hello": 3}

## FUNCTION PSEUDOCODE
# function count_word_frequencies(sentence):
    # words = split the sentence
    # counts = new empty dictionary
    # for each word in words
        # if the word is not in counts
            # counts[word] = 0
        # add one to counts[word]
    # return counts
#---------------------------------------------------------

#input: sentence: a string
#output: dict.  keys - words in the sentence
#               values - num of times that word appears in sentence
#

def count_word_frequencies(sentence):
    word_lst = (sentence.split()) #list of individual words
    word_counts = {}

    for word in word_lst:
        word = str(word)
        if str(word) not in word_counts:
            word_counts[word] = 1
        elif word in word_counts:
            word_counts[word] += 1

    return word_counts


print(count_word_frequencies("hello hello hello"))
