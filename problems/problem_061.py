# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]
#------------------------------------------------
#input: a list
#output: a copy of list w/out dupes

def remove_duplicates(list):
    no_dupes_list = []
    for item in list:
        if item not in no_dupes_list:
            no_dupes_list.append(item)
    return no_dupes_list

print(remove_duplicates([1,3,3,20,3,2,2]))
