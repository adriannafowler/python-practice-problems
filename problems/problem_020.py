# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.
#------------------------------------------------------------------

#is attendees >= (members_list * .5)
#parameters int
def has_quorum(attendees_list, members_list):
    if attendees_list >= (members_list * 0.5):
        return "More or exactly half are here"
    else:
        return "Less than half are here"
print(has_quorum(55, 50))
