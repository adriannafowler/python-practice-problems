# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.
#----------------------------------------------------------

# test each number to determine if they are > 0 and less than 11
#return True or False

def is_inside_bounds(x, y):
    #are x AND y inside bounds?
    if x >= 0 and x < 11 and y >= 0 and y < 11:
        return True
    else:
        return False

print(is_inside_bounds(0, 2.8))
