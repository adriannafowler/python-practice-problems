# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]
#-------------------------------------------------------
#input: list1 of numbers
#output: return list2 that only contains odd numbers from list1


def only_odds(numbers):
    odd_nums = []
    for num in numbers:
        if num % 2 == 0:
            continue
        else:
            odd_nums.append(num)
    return odd_nums

print(only_odds([1, 2, 3, 4, 5, 6, -1, 0, ]))
