# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7
#-------------------------------------------

#input: 2 nums
#output: sum of 2 nums

def sum_two_numbers(num1, num2):
    return num1 + num2


print(sum_two_numbers(2, 0))
