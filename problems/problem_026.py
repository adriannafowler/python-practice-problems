# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average
#-------------------------------------------------------


#input: list of numerical scores
# scores must be between 0-100

#averages the scores entered

#output: a letter grade average based on the numerical grade average

def calculate_grade(*values):
    sum = 0
    count = 0
    for value in values:
        if value < 0 or value > 100:
            return "Invalid input, value entered must be between 0 and 100"
        else:
            sum += value
            count += 1
    average = sum / count
    if average >= 90:
        return "A"
    elif average >= 80 and average < 90:
        return "B"
    elif average >= 70 and average < 80:
        return "C"
    elif average >= 60 and average < 70:
        return "D"
    else:
        return "F"

print(calculate_grade(81, 89, 88))
