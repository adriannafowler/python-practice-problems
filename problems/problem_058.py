# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.
#---------------------------------------------------------------

#input: list
#output: key:value pair in dict with key being 2 char state abrev.
#and value being a list with the city(ies)

def group_cities_by_state(cities):
    grouped_dict = {}
    for city in cities:
        name, state = city.split(",")
        state = state.strip()
        if state not in grouped_dict:
            grouped_dict[state] = []
        grouped_dict[state].append(name)
    return grouped_dict

print(group_cities_by_state(["Cleveland, OH", "Columbus, OH", "Chicago, IL"]))

#     index = 0
#     #spliting list at comma to deal with individual pairs
#     for item in cities_list:
#         split_list = cities_list[index].split(", ")
#         value_list = []
#         if split_list[1] in grouped_dict:
#             #grouped_dict[split_list[1]] needs to be a list to append
#             value_list.append(split_list[0])
#             #print(value_list)
#             #grouped_dict[split_list[1]] += (", " + split_list[0])
#             grouped_dict[split_list[1]] += value_list
#             #grouped_dict[new_key].append(new_value)
#         #grouped_dict[split_list[1]] = list()
#         else:
#         #adding key:value pair to new dict by indexing the list
#         #you created that now just contains one pair
#             grouped_dict[split_list[1]] = split_list[0]
#         index += 1
#     #for key in grouped_dict:
#         #grouped_dict[key] = list(grouped_dict.get(key))
#     return grouped_dict

# print(group_cities_by_state(["Cleveland, OH", "Columbus, OH", "Chicago, IL"]))

# #just need to get values in ["value1", "value1"] format


# def group_cities_by_state(cities):          # solution
#     output = {}                             # solution
#     for city in cities:                     # solution
#         name, state = city.split(",")       # solution
#         state = state.strip()               # solution
#         if state not in output:             # solution
#             output[state] = []              # solution
#         output[state].append(name)          # solution
#     return output
