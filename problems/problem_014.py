# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
#--------------------------------------------------------

# put ingredients in list
#check for list to include flour AND eggs AND oil
#with for loop
#return true or false

def can_make_pasta(ingredient1, ingredient2, ingredient3):
    ingredients = [ingredient1.lower(), ingredient2.lower(), ingredient3.lower()]
    ingredient_lst = []
    if "flour" in ingredients and "oil" in ingredients and "eggs" in ingredients:
        return True
    else:
        return False
    # for item in ingredients:
    #     if item == "flour":
    #         ingredient_lst.append("flour")
    #     if item == "eggs":
    #         ingredient_lst.append("eggs")
    #     if item == "oil":
    #         ingredient_lst.append("oil")
    #     if ingredient_lst == ["flour", "eggs", "oil"]:
    #         return True
    #     else:
    #         return False

print(can_make_pasta("flOUr", "OIl", "egGs"))
