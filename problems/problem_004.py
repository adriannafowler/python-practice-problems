# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
#-------------------------------------------------------------

#return max value or tied max value


def max_of_three(value1, value2, value3):
    #list of entered values
    values = value1, value2, value3
    #loop to compare each value to the list
    for value in values:
        if value >= value1 and value >= value2 and value >= value3:
            return value
        elif value == value1 and value == value2 and value == value3:
            return value


print(max_of_three(3.5,20,20.1))
