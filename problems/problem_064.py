# Write a function that meets these requirements.
#
# Name:       temperature_differences
# Parameters: highs: a list of daily high temperatures
#             lows: a list of daily low temperatures
# Returns:    a new list containing the difference
#             between each high and low temperature
#
# The two lists will be the same length
#
# Example:
#     * inputs:  highs: [80, 81, 75, 80]
#                lows:  [72, 78, 70, 70]
#       result:         [ 8,  3,  5, 10]
#-------------------------------------------------
#input: 2 lists: daily high temps, daily low temps
#output: new list with difference between each high and low temp

def temperature_differences(list_of_highs, list_of_lows):
    dif_list = []
    i = 0
    for temp in list_of_highs:
        dif = list_of_highs[i] - list_of_lows[i]
        dif_list.append(dif)
        i += 1
    return dif_list

print(temperature_differences([80, 81, 75, 80], [72, 78, 70, 70]))
