# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
#------------------------------------------------------


#out of 2 values, return the lesser of the 2
#if values are the same, return either
#parameters are integers, float


def minimum_value(value1, value2):
#less than comparison, if statement
    if value1 < value2:
        return value1
#elif opposite
    elif value2 < value1:
        return value2
#elif equal statement
    elif value1 == value2:
        return value1


print(minimum_value(4, 5))
