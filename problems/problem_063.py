# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem
#------------------------------------------------------
#input: string with a single word
#output: a new string with all letters replaced by next letter in alphabet
from string import ascii_letters

def shift_leaders(string):
    new_string = ""
    for char in string:
        if char in ascii_letters:
            new_string = new_string + ascii_letters[(ascii_letters.index(char)+1) % len(ascii_letters)]
        else:
            new_string += char
    return new_string

print(shift_leaders("Kala"))

# def shift_letters(word):                        # solution
#     new_word = ""                               # solution
#     for letter in word:                         # solution
#         if letter == "Z":                       # solution
#             new_letter = "A"                    # solution
#         elif letter == "z":                     # solution
#             new_letter = "a"                    # solution
#         else:                                   # solution
#             new_letter = chr(ord(letter) + 1)   # solution
#         new_word += new_letter                  # solution
#     return new_word
