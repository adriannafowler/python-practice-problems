# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"
#-------------------------------------------------------------

#input: password
#check criteria, must meet all requirements
#output: "good password", "bad password, check requirements,
#and try again"

def check_password(password):
    lowercase = []
    uppercase = []
    digit = []
    special = []
    length = int(len(password))

    for char in password:
        if char.islower():
            lowercase.append(char)
        if char.isupper():
            uppercase.append(char)
        if char.isdigit():
            digit.append(char)
        if char == "$" or char == "!" or char == "@":
            special.append(char)

    if length >= 6 and length <= 12:
        length = True
    else:
        length = False

    if lowercase and uppercase and digit and special and length:
        return "Good Password"
    else:
        return "bad password, check requirements, and try again"

print(check_password("uuUU!!122"))
