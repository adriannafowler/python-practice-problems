# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]
#----------------------------------------------------

#input: 1 list
#output: 2 lists --> each half of the original
#if og list has odd number of items then extra item goes to first new list

def halve_the_list(list):
    new_list1 = []
    new_list2 = []
    list_length = len(list)
    list1_length = 0
    list2_length = 0

    if list_length % 2 != 0:
        list1_length = (list_length // 2) + 1
        list2_length = (list_length // 2)
    else:
        list1_length = (list_length // 2)
        list2_length = (list_length // 2)

    for i in range(list1_length):
        new_list1.append(list[i])
    for i in range(list2_length):
        index = i + list1_length
        new_list2.append(list[index])


    #new_list1.append(list[0: list1_length])

    #new_list2.append(list[(list_length - list1_length): (list_length + 1)])

    #new_list1.append(list[0: list1_length])
    #new_list2.append(list[list2_length : (list_length + 1)])
    #print(list1_length, list2_length)
    #print(list_length - list1_length)
    print(new_list1, new_list2)

print(halve_the_list([1, 2, 3]))





def halve_the_list(input):                              # solution
    return (                                            # solution
        input[0:len(input) // 2 + (len(input) % 2)],    # solution
        input[len(input) // 2 + (len(input) % 2):],     # solution
    )                                                   # solution
                                                        # noqa # solution
# or                                                    # solution
                                                        # noqa # solution
def halve_the_list(input):                              # noqa # solution
    first_list = []                                     # solution
    second_list = []                                    # solution
    first_list_len = len(input) // 2 + (len(input) % 2) # noqa # solution
    for i in range(first_list_len):                     # solution
        first_list.append(input[i])                     # solution
    for i in range(len(input) // 2):                    # solution
        index = i + first_list_len                      # solution
        second_list.append(input[index])                # solution
    return first_list, second_list                      # solution
