# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
#--------------------------------------------------------


def fizzbuzz(number):
#use mod to see if remainder for / by 3 and / by 5 is 0
# if True, return "fizzbuzz"
    if number % 3 == 0 and number % 5 == 0:
        return "fizzbuzz"
    #check for divisible by 3 --> if True, return "fizz"
    elif number % 3 == 0 and number % 5 != 0:
        return "fizz"
    #check for divisible by 5 --> if True, return "buzz"
    elif number % 5 == 0 and number % 3 != 0:
        return "buzz"
    #everything else --> return number
    else:
        return number

print(fizzbuzz(12))
