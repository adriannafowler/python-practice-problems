from arepl_dump import dump
# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you
#--------------------------------------------------------

#parameter, a list - numerical
#if list empty return none

def calculate_average(*values):
    #for loop
    count = 0
    sum = 0
    for value in values:
        count += 1
        sum += value
    if list == None:
        return None
    else:
        return (sum/count)

print(calculate_average(2, 3, 3, 5, 7, 10))
